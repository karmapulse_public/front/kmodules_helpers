function findHashtagInBodyText(text) {
    const regex = /(^#|[^&]#)([a-z0-9]+[^\s!@#$%^&*()=+./,{\]]+)/gi;
    return text.replace(regex, (hashtag) => {
        const h = (hashtag.replace('#', '')).trim();
        return `<a href="https://twitter.com/hashtag/${h}" target="_blank">${hashtag}</a>`;
    });
}

export default findHashtagInBodyText;
