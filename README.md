# KarmaModules Helpers

En este repositorio se encuentra una librería de funciones
de utilidad para los proyectos de KarmaPulse.

La definición de su estructura detalladamente se encuentra [aquí](https://karmapulse.atlassian.net/wiki/spaces/frontend/pages/68288516/Estructura+de+archivos+para+Helpers).

Para instalarlo, ejecutar en la consola:
```
npm install
```
ó
```
yarn install
```

Para hacer testing de las funciones, ejecutar en la consola:
```
npm test
```
ó
```
yarn test
```
