function findUrlInBodyText(text) {
    const regex = /(https?:\/\/[^\s]+)/g;
    return text.replace(regex, (url) => {
        const URL = url;
        return `<a href="${URL}" target="_blank">${URL}</a>`;
    });
}

export default findUrlInBodyText;
