function findUserInBodyText(text) {
    const regex = /(^|\s)@(\w+)/g;
    const regex1 = /(^|\s)@(\w+)/g;
    const tweet = text.replace(regex, '$1<a href="http://www.twitter.com/$2" target="_blank">@$2</a>');
    return tweet.replace(regex1, '$1#<a href="http://search.twitter.com/search?q=%23$2" target="_blank" >"@$2</a>');
}

export default findUserInBodyText;
