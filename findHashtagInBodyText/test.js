import findHashtagInBodyText from './index';

test('Coloca links a hashtags', () => {
    expect(
        findHashtagInBodyText('#FelizViernes')
    ).toBe(
        '<a href="https://twitter.com/hashtag/FelizViernes" target="_blank">#FelizViernes</a>'
    );
});
