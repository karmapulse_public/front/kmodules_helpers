import formatBodyText from './index';

test('Coloca links a menciones de usuarios', () => {
    expect(
        formatBodyText('@alfredodelmazo @Ecatepec Excelente trabajo')
    ).toBe(
        '<a href="http://www.twitter.com/alfredodelmazo" target="_blank">@alfredodelmazo</a> <a href="http://www.twitter.com/Ecatepec" target="_blank">@Ecatepec</a> Excelente trabajo'
    );
});
