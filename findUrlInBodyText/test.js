import findUrlInBodyText from './index';

test('Coloca links a texto en forma de url', () => {
    expect(
        findUrlInBodyText('https://t.co/hUUJJL7vLH')
    ).toBe(
        '<a href="https://t.co/hUUJJL7vLH" target="_blank">https://t.co/hUUJJL7vLH</a>'
    );
});
