import isUndefined from 'lodash/isUndefined';

import findUrlInBodyText from '../findUrlInBodyText';
import findHashtagInBodyText from '../findHashtagInBodyText';
import findUserInBodyText from '../findUserInBodyText';

function formatBodyText(tweet) {
    if (!isUndefined(tweet)) {
        const withUrl = findUrlInBodyText(tweet);
        const withHashtag = findHashtagInBodyText(withUrl);
        return findUserInBodyText(withHashtag);
    }
    return '';
}

export default formatBodyText;
