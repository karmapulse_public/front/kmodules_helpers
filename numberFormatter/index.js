import numberWithCommas from '../numberWithCommas';

function numberFormatter(num) {
    if (num >= 1000000000) {
        const fnum = (num / 1000000000).toFixed(1).replace(/\.0$/, '');
        return `${numberWithCommas(fnum)}G`;
    }
    if (num >= 1000000) {
        const fnum = (num / 1000000).toFixed(1).replace(/\.0$/, '');
        return `${numberWithCommas(fnum)}M`;
    }
    if (num >= 1000) {
        const fnum = (num / 1000).toFixed(1).replace(/\.0$/, '');
        return `${numberWithCommas(fnum)}K`;
    }
    return num;
}

export default numberFormatter;
