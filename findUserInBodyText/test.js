import findUserInBodyText from './index';

test('Coloca links a menciones de usuarios', () => {
    expect(
        findUserInBodyText('@SusCasasola')
    ).toBe(
        '<a href="http://www.twitter.com/SusCasasola" target="_blank">@SusCasasola</a>'
    );
});
